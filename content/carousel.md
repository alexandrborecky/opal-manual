---

title: 'Premium Carousel'
description: 'Premium Carousel obohacuje váš web nejen ze vzhledového hlediska, ale také z toho UX (uživatelská zkušenost). Pravá část zobrazuje veškerý textový obsah, který se v carouselu skrývá a zároveň funguje jako navigace skrze jednotlivé části banneru. Je tak perfektním doplněním vašeho e-shopu, neboť takovéto detaily vytváří zajímavou uživatelskou zkušenost.

'

introChapter: 'Úvod'
chapterOne: 'Dark mode'
chapterTwo: 'Skrytí tlačítek na telefonech'
chapterThree: 'Poměr stran fotografie'

introSubtitle: 'Základní vlastnosti Premium Carouselu'
introSubtextOne: 'Díky informacím vypsaným v pravé části, má uživatel okamžite na očích vše, co je v Carouselu obsaženo a mohou si tak vybrat informaci, která je zajímá nejvíc. Má tak kontrolu nad obsahem a prohlížením webu, což je pro celkovou zkušenost a prožitek z webu zásadní.'
snackBarWarning: 'Podoba Carouselu jak jí vidíte na obrázku je možná díky nastavením, které najdete v tomto manuálu'

darkTitle: 'Dark mode'
darkDescription: 'Dark mode je tmavý vzhled originálního Premium carouselu, který se skvěle hodí na e-shopy s tmavším tématem, nebo větším kontrastem barev. Jeho nastavení je poměrně snadné a naleznete jej níže'

stepOne: 'Krok 1: Administrace Shoptetu'
oneText: 'V první řadě budeme chtít přidat kód do patřičného místa v naší Shoptet administraci. Toto místo najdeme následovně:'
snippetOne: 'VZHLED A OBSAH > Editor > HTML Kód'

stepTwo: 'Krok 2: Přidání stylu'
twoText: 'Zkopírujte kód níže do pole Záhlaví (před koncovým tagem HEAD)'

stepThree: 'Krok 3: Uložit a obnovit stránku'
threeText: 'Po uložení obnovte stránku svého e-shopu a změna se projeví ihned.' 

btnDescription: 'Toto nastavení vám pmůže vypnout tlačítka Carouselu na mobilních zařízeních, konkrétně na telefonech a zařízeních s menším rozlišením.'

---