---

description: 'Celestin obsahuje výpis kategorií na hlavní straně. Ty je samozřejmě možné nepřidávat, nicméně vřele doporučujeme jejich nastavení. Můžete tak oživit svojí hlavní stranu a přidat možnost navigace na sekce, které jsou pro vás důležité.'

stepOne: 'Krok 1: Administrace Shoptetu'
textOne: 'Jak je u nás zvykem, je třeba se dostat do sekce, kde můžeme kategorie nastavit, a to následovně:'
snippetOne: 'VZHLED & OBSAH > Články'

stepTwo: 'Krok 2: Přidání rubriky'
twoText: 'Jakmile se dostaneme do kategorie článku, musime přidat novou rubriku a pojemnovat ji HP Kategorie'
snackOne: 'Je důležité, aby rubrika byla pojmenováná HP Kategorie pro správné fungování navigace.'

stepThree: 'Krok 3: Přidání článku (kategorie)'
threeText: 'V nově vytvořené kategorii můžeme přidat článek. Jeho pojemnování bude stejné, jako jméno kategorie, která bude zobrazena na hlavní straně.'

stepFour: 'Krok 4: Nastavení článku (kategorie)'
fourText: 'Pro správné fungování je opět nutné mít nastavení tak, jak jej vidíte na obrázku níže.'

snack: 'Je opravdu podstatné, aby tyto kroky byly dodrženy. Pouze tak bude možné přidat kategorie správně. Ujistěte se také, že jsou vaše články v rubrice viditelné.'

snackTwo: 'Další kategorie přidáte opakováním tohoto procesu.'

---