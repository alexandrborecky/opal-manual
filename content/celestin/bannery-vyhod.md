---

description: 'V této kapitole vás provedeme nastavením Bannerů výhod, které naleznete na hlavní straně'


stepOne: 'Krok 1: Administrace Shoptetu'
oneText: 'V první řadě je důležité nalézt sekci, kam budeme bannery přidávat. Tuto část administrace najdete podle cesty níže.'
snippetOne: 'VZHLED & OBSAH > Bannery > Doplňkové bannery > Přidat'

stepTwo: 'Krok 2: Nastavení banneru'
twoText: 'Jakmile je banner přidán, nastavní provedme podle obrázku níže. Konkrétně se soustřeďme na TYP BANNERU, POZICE BANNERU a zviditelnění na všech zařízeních.'

stepThree: 'Krok 3: Upravovaní kódu'
threeText: 'Podívejme se nejdříve na kód, který budeme přidávat do editoru našeho nově přidaného banneru'

stepFour: 'Krok 4: Přidání kódu'
fourText: 'Nyní známe atributy, které můžeme upravovat a nic nám tedy nebrání nahrít kód do administrace Shoptetu k nově přidanému banneru, a to do textové sekce Nastavení.'

snack: 'Další výhody do svéh banneru přidáte opakováním stejného procesu.'
snackTwo: 'Vyzkoušejte náš generátor kódu, který vám pomužeme vyplnit kód dle vašich potřeb'

---
