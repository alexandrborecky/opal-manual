---

description: 'A jako bonus jsme si pro vás připravili tématickou vychytávku, která neodmyslitelně patří k Vánocům - padání sněhu ❄️! Jde o jednoduchou animaci, kdy vločky sněhu padají na vašem e-shopu dolů, směrem od hlavní navigace. Nastavení je velmi podobné nastavení dynamckých bannerů, nebo Light Mode.'

---