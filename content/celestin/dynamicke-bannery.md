---

description: 'Nastavením dynamických bannerů umožníme automatickou úpravu velikosti obrázků, které zadáváme jako hlavní na naší domácí stránku. Toto nastavení je velmi snadné a je možné jej provést ve dvou krocích'

stepOne: 'Krok 1: Administrace Shoptetu'
textOne: 'V administraci se musíme dostat do sekce editoru kódu, a to pomocí následující cesty.'

snippetOne: 'VZHLED & OBSAH > Editor > HTML kód'

stepTwo: 'Krok 2: Přidání kódu'
twoText: 'Jakmile jsme v sekci editoru HTML kódu, zkopírujeme kód uvedený níže do sekce Záhlaví (před koncovým tagem HEAD) '



---