---

title: 'Celestin'
description: 'Celestin je nová Blank šablona z díly Shopteťák.cz, která se opět zaměřuje na e-shopy s středním a větším množstvím produktů v nabídce. Šablona je propracovaná do nejmenších detailů, obohacená o UX šité na míru podobnému typu šablon a především disponuje čistým designem, který si vaši zákaznící zamilují, a to nově hned ve dvou variantách!'



introChapter: 'Úvod'
chapterOne: 'Bannery výhod'
chapterTwo: 'Dynamické bannery'
chapterThree: 'Kategorie na hlavní straně'
chapterFour: 'Light Mode'
chapterFive: 'Winter Mode'

subtitle: 'Dokumenty'
textOne: 'V následujících několika kapitolách vás provedeme nastavením různých funkcí, které jsme pro vás v šabloně Celestin připravili.'
snack: 'V případě, že byste nenašli některé nastavení šablony, neváhejte nás kontaktovat'


---